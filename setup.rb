require 'active_record'


ActiveRecord::Base.establish_connection({
                                          :adapter => 'mysql2',
                                          :database => 'robin',
                                          :host => 'localhost',
                                          :username => 'root',
                                          :password => "admin"
                                        })


class WpTerm < ActiveRecord::Base
  self.table_name = 'wp_terms'
end

class WpPost < ActiveRecord::Base
end

class WpPostMeta < ActiveRecord::Base
  self.table_name = 'wp_postmeta'
end

class WpTermRelationship < ActiveRecord::Base
end

class WpTermTaxonomy < ActiveRecord::Base
  self.table_name = 'wp_term_taxonomy'
end





require 'csv'

class CsvSource

  def initialize(input_file)
    @csv_obj = CSV.open(input_file, headers: true, col_sep: "\t")
    @data    = @csv_obj.to_a
    @limit   = 0
  end

  def each
    @data.each do |row|
      # break if @limit > 10; @limit += 1

      next if row.size == 0
      yield(row.to_hash)
    end
    @csv_obj.close
  end

end

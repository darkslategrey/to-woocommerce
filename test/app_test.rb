require 'test_helper'

describe 'kiba' do
  it 'can be instanciated' do
    filename = 'app.etl'
    job_def  = Kiba.parse(IO.read(filename), filename)
    # job_def  = Kiba.parse('', filename)
    Kiba.run(job_def)
  end
end

$VERBOSE=nil

require 'rubygems'
require 'bundler/setup'

require 'minitest/autorun'
# $LOAD_PATH.unshift 'lib'

require_relative '../app'

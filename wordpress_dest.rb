require_relative 'setup'
require 'pry'
require 'active_support'
require 'active_support/core_ext/string'

class WordPressDest

  def write(row)

    =begin
       TERMS / CATEGORIES
       cat_1 = WpTerm.find_or_create(name: row[25].humanize, slug: row[25].parameterize)
       cat_2 = WpTerm.find_or_create(name: row[27].humanize, slug: row[27].parameterize)
       cat_3 = WpTerm.find_or_create(name: row[28].humanize, slug: row[28].parameterize)
     end

    =begin
       TERM_RELATIONSHIPS

       =end

    =begin
       TERM_TAXONOMY
       taxo1 = WpTermToxnonomy.find_or_create(term_id: cat_1.term_id, taxonomy: 'product_cat',
                                              parent: 0)
       taxo2 = WpTermToxnonomy.find_or_create(term_id: cat_2.term_id, taxonomy: 'product_cat',
                                              parent: taxo1.term_taxonomy_id)
       taxo3 = WpTermToxnonomy.find_or_create(term_id: cat_3.term_id, taxonomy: 'product_cat',
                                              parent: taxo2.term_taxonomy_id)
       taxo1.update(count: taxo1.count + 1)
       taxo2.update(count: taxo2.count + 2)
       taxo3.update(count: taxo3.count + 3)

     =end
    =begin
       POSTMETA
       post_meta_params = {
         _tax_class: row[21] == 20 ? nil : 'reduced-rate',
	       _tax_status: 	taxable,
         _edit_last: 1,
         _edit_lock: nil,
         _visibility: 'visible',
         _stock_status: row[32] > 0 : 'instock' : 'outofstock',
         total_sales: 0,
         _downloadable: 'no',
         _virtual: 'no'
         _purchase_note: '',
         _featured: 'no',
         _weight: row[11],
         _length: nil,
         _width: nil,
         _height: nil,
         _sku: row[0],
         _product_attributes:
           a:3:{s:13:"pa_contenance";a:6:{s:4:"name";s:13:"pa_contenance";s:5:"value";s:0:"";s:8:"position";s:1:"0";s:10:"is_visible";i:1;s:12:"is_variation";i:1;s:11:"is_taxonomy";i:1;}

                 s:10:"pa_couleur";a:6:{s:4:"name";s:10:"pa_couleur";s:5:"value";s:0:"";s:8:"position";s:1:"1";s:10:"is_visible";i:1;s:12:"is_variation";i:1;s:11:"is_taxonomy";i:1;}

                 s:9:"pa_parfum";a:6:{s:4:"name";s:9:"pa_parfum";s:5:"value";s:0:"";s:8:"position";s:1:"2";s:10:"is_visible";i:1;s:12:"is_variation";i:1;s:11:"is_taxonomy";i:1;}}

       _regular_price
       _sale_price
       _sale_price_dates_from
       _sale_price_dates_to
       _price
       _sold_individually
       _manage_stock
       _backorders
       _stock
       _upsell_ids
       _crosssell_ids
       _product_version
       _product_image_gallery
       _wc_rating_count
       _wc_review_count
       _wc_average_rating
    =end

    =begin
       POST
       params = {
         post_author: 1,
         post_title: row[2],
         post_status: 'publish',
         post_type: 'product',
         menu_order: 0,
         post_content: row[4],
         post_excerpt: row[3],
       }
       post = WpPost.find_by_post_title(row[2])
       variation_n = ''
       if post.size > 1
         variation_n = "-#{post.size}"
       end
       if not post.nil? # variation
         params = {
           post_author: 1,
           post_title: "Variation ##{post.ID + 1} of #{post.title}",
           post_status: 'publish',
           post_name: "product-#{post.ID}-variation#{variation_n}",
           post_parent: post.ID,
           guid: "http://robin.local/product_variation/product-#{post.ID}-variation#{variation_n}/",
           menu_order: 1,
           post_type: 'product_variation'
         }
         variation_post = WpPost.create(params)

         meta_params = {
           post_id: variation_post.ID,
           _sku: row[34],
           _thumbnail_id:   0,
           _virtual:       'no',
           _downloadable:   'no',
           _weight: row[11],
           _length: nil,
           _width: nil,
           _height: nil,
           _manage_stock:   'yes',
           _backorders:      'no',
           _stock: row[32],
           _stock_status:   row[32] > 1 ? 'instock' : 'outofstock',
           _regular_price: row[35],
           _sale_price: nil,
           _sale_price_dates_from: nil,
           _sale_price_dates_to: nil,
           _price: row[35],
           _download_limit: nil,
           _download_expiry: nil,
           _downloadable_files: nil,
           _variation_description: nil
         }

         attr_meta = { }
         if not row[5].strip.blank?
           attr_meta[:attribute_pa_contenance] = row[5]
           meta_params[:_price] = row[6]
         end

         if not row[7].strip.blank?
           attr_meta[:attribute_pa_parfum] = row[7]
           meta_params[:_price] = row[8]
         end

         if not row[9].strip.blank?
           attr_meta[:attribute_pa_couleur] = row[9]
         end

         if not row[13].strip.blank?
           attr_meta[:attribute_pa_poids] = row[13]
           meta_params[:_price] = row[14]
         end

         if not row[19].strip.blank?
           attr_meta[:attribute_pa_taille] = row[19]
         end

         meta_params.merge!(attr_meta)

         WpPostmeta.create(meta_params)
       end

     end
  end

  def close
    ActiveRecord::Base.clear_active_connections!
  end

  def create_post(row)
    post
  end

end
